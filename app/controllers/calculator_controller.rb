class CalculatorController < ApplicationController
  def index
  end
  
  def calculate
    @result = Calculator.send(params[:operation], *[params[:a], params[:b]])
    render :index
  end
end
