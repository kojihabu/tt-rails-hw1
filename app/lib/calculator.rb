class Calculator
  def self.add(a, b)
    return a.to_i + b.to_i
  end

  def self.subtract(a, b)
    return a.to_i - b.to_i
  end

  def self.multiply(a, b)
    return a.to_i * b.to_i
  end

  def self.divide(a, b)
    return a.to_i / b.to_i
  end
end
