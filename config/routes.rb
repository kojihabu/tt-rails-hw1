Rails.application.routes.draw do
  get 'welcome/index'

  root 'welcome#index'

  match "/calculator", to: "calculator#index", :via => 'get'

  get 'calculator/calculate'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
